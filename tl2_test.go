package stm_test

import (
	"fmt"
	"math/rand"
	"slices"
	"sort"
	"sync"
	"testing"

	"codeberg.org/chensl/stm"
	"github.com/stretchr/testify/assert"
)

func ExampleAtomically_counter() {
	cnt := stm.NewTVar(0)
	var new int
	err := stm.Atomically(func(tx *stm.Tx) error {
		old, err := cnt.Load(tx)
		if err != nil {
			return err
		}
		new = old.(int) + 1
		cnt.Store(tx, new)
		return nil
	})
	fmt.Println(new, err)

	//Output:
	//1 <nil>
}

func ExampleAtomically_pickRandomNumber() {
	xs := stm.NewTVar([]int{1, 2, 3, 4, 5})

	pick := func(x *int) func(tx *stm.Tx) error {
		return func(tx *stm.Tx) error {
			v, err := xs.Load(tx)
			if err != nil {
				return err
			}
			old := slices.Clone(v.([]int)) // avoid side-effect
			idx := rand.Intn(len(old))
			*x = old[idx]
			new := slices.Delete(old, idx, idx+1)
			xs.Store(tx, new)
			return nil
		}
	}

	ch := make(chan int)

	for i := 0; i < 5; i++ {
		go func() {
			var x int
			_ = stm.Atomically(pick(&x))
			ch <- x
		}()
	}

	var res []int
	for i := 0; i < 5; i++ {
		res = append(res, <-ch)
	}
	sort.Ints(res)
	fmt.Println(res)

	//Output:
	//[1 2 3 4 5]
}

func TestAtomically(t *testing.T) {
	cnt := stm.NewTVar(0)

	incr := func(tx *stm.Tx) error {
		old, err := cnt.Load(tx)
		if err != nil {
			return err
		}
		cnt.Store(tx, old.(int)+1)
		return nil
	}

	var wg sync.WaitGroup
	for i := 0; i < 4; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for j := 0; j < 10000; j++ {
				_ = stm.Atomically(incr)
			}
		}()
	}
	wg.Wait()

	var actual int
	err := stm.Atomically(func(tx *stm.Tx) error {
		iface, err := cnt.Load(tx)
		if err != nil {
			return err
		}
		actual = iface.(int)
		return nil
	})
	assert.NoError(t, err)
	assert.Equal(t, 40000, actual)
}
