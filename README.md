# Software Transactional Memory

This implementation is based on the paper: [Transactional Locking II - D Dice](https://dcl.epfl.ch/site/_media/education/4.pdf).

```go
cnt := stm.NewTVar(0)
new, err := stm.Atomically(func(tx *stm.Tx) (any, error) {
    old, err := cnt.Load(tx)
    if err != nil {
        return nil, err
    }
    new := old.(int) + 1
    cnt.Store(tx, new)
    return new, nil
})
```

See [tl2_test.go](./tl2_test.go) for more examples.
