package stm

import (
	"errors"
	"sync"
	"sync/atomic"
)

const (
	_isLocked    = (1 << 63)
	_versionMask = (1 << 63) - 1
)

var globalVersionClock versionClock

var txPool = sync.Pool{
	New: func() any {
		return new(Tx)
	},
}

type TVar struct {
	v  atomic.Value
	mu versionedWriteLock
}

func NewTVar(init any) *TVar {
	tv := &TVar{}
	tv.v.Store(init)
	return tv
}

func (v *TVar) Load(tx *Tx) (any, error) {
	if v, ok := tx.writeSet[v]; ok {
		return v, nil
	}

	locked, version1 := v.mu.load()
	if locked || version1 > tx.rv {
		return nil, ErrConflict
	}

	val := v.v.Load()

	locked, version2 := v.mu.load()
	if version1 != version2 || version2 > tx.rv || locked {
		return nil, ErrConflict
	}

	tx.readSet = append(tx.readSet, v)
	return val, nil
}

func (v *TVar) Store(txn *Tx, val any) {
	if txn.writeSet == nil {
		txn.writeSet = make(map[*TVar]any)
	}
	txn.writeSet[v] = val
}

type Tx struct {
	writeSet map[*TVar]any
	readSet  []*TVar
	locked   []*TVar
	rv       uint64
}

func Atomically(f func(tx *Tx) error) error {
	tx := txPool.Get().(*Tx)
	defer func() {
		tx.readSet = tx.readSet[:0]
		tx.locked = tx.locked[:0]
		clear(tx.writeSet)
		txPool.Put(tx)
	}()

	for {
		tx.rv = globalVersionClock.load()
		if err := tx.run(f); err != nil {
			if errors.Is(err, ErrConflict) {
				tx.readSet = tx.readSet[:0]
				if len(tx.locked) > 0 {
					for _, writeVar := range tx.locked {
						writeVar.mu.unlock()
					}
					tx.locked = tx.locked[:0]
				}
				clear(tx.writeSet)
				continue
			}
			return err
		}
		return nil
	}
}

func (tx *Tx) run(f func(tx *Tx) error) error {
	if err := f(tx); err != nil {
		return err
	}

	if len(tx.writeSet) == 0 {
		return nil
	}

	if tx.locked == nil {
		tx.locked = make([]*TVar, 0, len(tx.writeSet))
	}
	for writeVar := range tx.writeSet {
		if !writeVar.mu.tryLock() {
			return ErrConflict
		}
		tx.locked = append(tx.locked, writeVar)
	}

	writeVersion := globalVersionClock.incr()
	if writeVersion != tx.rv+1 {
		for _, readVar := range tx.readSet {
			locked, version := readVar.mu.load()
			var lockedByMe bool
			if locked {
				_, lockedByMe = tx.writeSet[readVar]
			}
			if locked && !lockedByMe || version > tx.rv {
				return ErrConflict
			}
		}
	}

	for writeVar, new := range tx.writeSet {
		writeVar.v.Store(new)
		writeVar.mu.commit(writeVersion)
	}
	return nil
}
