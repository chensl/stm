package stm

import "sync/atomic"

type versionedWriteLock struct {
	state atomic.Uint64
}

func splitState(state uint64) (bool, uint64) {
	return state&_isLocked != 0,
		state & _versionMask
}

func joinState(locked bool, version uint64) uint64 {
	if locked {
		return _isLocked | version
	}
	return version
}

func (l *versionedWriteLock) tryLock() bool {
	old := l.state.Load()
	locked, version := splitState(old)
	if locked {
		return false
	}
	new := joinState(true, version)
	return l.state.CompareAndSwap(old, new)
}

func (l *versionedWriteLock) unlock() {
	locked, version := splitState(l.state.Load())
	if !locked {
		panic("stm: unlock of unlocked mutex")
	}
	l.state.Store(joinState(false, version))
}

func (l *versionedWriteLock) commit(version uint64) {
	locked, _ := splitState(l.state.Load())
	if !locked {
		panic("stm: unlock of unlocked mutex")
	}
	l.state.Store(joinState(false, version))
}

func (l *versionedWriteLock) load() (bool, uint64) {
	return splitState(l.state.Load())
}
