package stm

import "errors"

var ErrConflict = errors.New("stm: transaction conflict, please retry")
