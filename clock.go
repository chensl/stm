package stm

import "sync/atomic"

type versionClock struct {
	id atomic.Uint64
}

func (c *versionClock) load() uint64 {
	return c.id.Load()
}

func (c *versionClock) incr() uint64 {
	return c.id.Add(1)
}
